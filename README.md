# Travail pratique 1

## Description


Dans ce premier travail pratique, nous devons concevoir un petit programme C qui prend en entrée quatre arguments et qui dessine, en fonction de ces arguments, un chemin dans un rectangle. Plus précisément, on s'attend à ce que la commande affiche sur stdout une représentation textuelle d'un chemin évoluant dans un rectangle.
Notre programe devra prendre en entrée trois caractères (séparés par des espaces) suivis d'une chaîne de caractère représentant un chemin utilisant les quatre points cardinaux (E, N, W et S).
Le projet a été réalisé dans le cadre du cours INF3135 Construction et maintenance de logiciel de la session d’hiver2017 à l’Université du Québec A Montréal donné par le professeur  Alexandre Blondin Massé.

## Auteur

Dirima Verdi Kanekatoua (KAND01099205)

## Fonctionnement

Pour générer un exécutable du programme, il suffit d’entrer la commande

    make

Puis on lance le programme à l’aide de la commande

    ./tp1 <o> <v> <i> <c>

    où

    o est un caractère représentant une case occupée;
    v est un caractère représentant une case vide;
    i est un caractère représentant une intersection et
    c est une chaîne de caractère représentant une suite de déplacements selon les quatre points cardinaux (E pour est, N pour nord, W pour ouest et S pour sud).

- Exemple 1:

    ./tp1 x - + NNNSSEEEWWW

- Resutat affiché:

    x---                               
    x---                                
    +xxx                                
    x---


- Exemple 2:

    ./tp1 a + - SSNNSWWWEEEN

- Resutat affiché:

    a++a                               
    -aaa                                
    a+++                                


Lorsqu'une commande est lancée, il est possible que l'utilisateur ne respecte pas certaines contraintes. Afin de nous assurer de la robustesse de notre programme, nous avons effectué certaines vérifications pour qu'il n'ait pas un comportement non souhaité :

- Il doit toujours y avoir exactement 4 arguments passés en paramètres.
- Les trois arguments représentant une case occupée, une case vide et une intersection doivent être des caractères uniques (bref, des chaînes de caractères de longueur 1).
- Les trois caractères représentant une case occupée, une case vide et une intersection doivent être distincts deux à deux.
- Le chemin doit être une suite de caractères choisis parmi E, N, S et W, en étant sensible à la casse (bref, la lettre minuscule e, par exemple, n'est pas accepté).
- La longueur de la chaîne représentant le chemin ne doit pas dépasser 40.
- La hauteur (le nombre de lignes) du chemin ne doit pas dépasser 10.
- La largeur (le nombre de colonnes) du chemin ne doit pas dépasser 15.
- Le chemin doit toucher les quatre côtés du rectangle (il ne peut donc pas y avoir de colonnes ou de lignes vides.
- L'affichage doit terminer par un retour à la ligne (caractère '\n').

Les fichiers `.o` et l'exécutable doivent être supprimés en entrant la commande

    make clean

Une suite de tests est lancée pour valider notre programme lorsqu'on entre la commande

    make test


## Contenu du projet


Les fichiers contenus dans le projet sont:

- Un fichier `tp1.c` contenant le code source du projet, ainsi la fonction main.
- Un fichier `README.md` qui en décrit le contenu;
- Un fichier `Makefile` supportant les appels make, make clean et make test;
- Un fichier `.gitignore`;
- Les fichiers `test.py` qui contient une suite de tests écrite en Python (vous n'avez pas besoin de connaître ce langage pour la faire fonctionner); et `gitlab-ci.yml` qui indique à GitLab comment lancer de façon automatique la suite de tests.


## Références

Structures de données et prototypes du **Professeur** : Alexandre Blondin Massé.


## Statut

Le projet est complet et sans bogue.
