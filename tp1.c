#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// ---------- //
// Constantes //
// ---------- //

#define MAX_LIGNES   10
#define MAX_COLONNES 15
#define MAX_CHEMIN   40
#define EST          'E'
#define NORD         'N'
#define OUEST        'W'
#define SUD          'S'

// --------------------- //
// Structures de données //
// --------------------- //

enum Code {                             // Code de validation des arguments
    CODE_OK                        = 0, // Tout est OK
    CODE_NOMBRE_ARGUMENTS_INVALIDE = 1, // Le nombre d'arguments est invalide
    CODE_CARACTERES_NON_UNIQUES    = 2, // Problème pour identifier les cases
    CODE_CARACTERE_VIDE_INVALIDE   = 3, // Le caractère vide est égal à un autre
    CODE_CHEMIN_INVALIDE           = 4, // Le chemin utilise des lettres invalides
    CODE_CHEMIN_TROP_LONG          = 5, // La longueur du chemin est trop grande
    CODE_HAUTEUR_INVALIDE          = 6, // La hauteur du chemin n'est pas valide
    CODE_LARGEUR_INVALIDE          = 7, // La largeur du chemin n'est pas valide
};

struct Rectangle { // Un rectangle à coordonnées entières
    int xmin;      // L'abscisse minimale
    int xmax;      // L'abscisse maximale
    int ymin;      // L'ordonnée minimale
    int ymax;      // L'ordonnée maximale
};

struct Dessin {                           // Un dessin
    unsigned int nbLignes;                // Le nombre de lignes
    unsigned int nbColonnes;              // Le nombre de colonnes
    char cases[MAX_LIGNES][MAX_COLONNES]; // Les cases du dessin
    char occupe;                          // Le caractère pour une case occupée
    char vide;                            // Le caractère pour une case vide
    char intersection;                    // Le caractère pour une intersection
};

struct Chemin {                        // Un chemin
    struct Rectangle rectangle;        // Le rectangle dans lequel s'inscrit le chemin
    char deplacements[MAX_CHEMIN + 1]; // Les déplacements
};

// ---------- //
// Prototypes //
// ---------- //

int afficherErreur(enum Code code);
int cheminEstValide(const char *chemin);
void initialiserDessin(struct Dessin *dessin);
void afficherDessin(const struct Dessin *dessin);
void initialiserChemin(struct Chemin *chemin, const char *deplacements);
void ajouterCheminDansDessin(struct Dessin *dessin, const struct Chemin *chemin);
void identifierIntersections(struct Dessin *dessin);

//affichage des erreurs et retour des codes
int afficherErreur(enum Code code) {
    switch (code) {
        case CODE_NOMBRE_ARGUMENTS_INVALIDE:
        printf("Erreur: le nombre d'arguments est invalide\n");
        return CODE_NOMBRE_ARGUMENTS_INVALIDE;

        case CODE_CARACTERES_NON_UNIQUES:
        printf("Erreur: les cases doivent etre identifiees par des caracteres\n");
        return CODE_CARACTERES_NON_UNIQUES;

        case CODE_CARACTERE_VIDE_INVALIDE:
        printf("Erreur: le caractere de case vide doit etre distinct\n");
        return CODE_CARACTERE_VIDE_INVALIDE;

        case CODE_CHEMIN_INVALIDE:
        printf("Erreur: les deplacements doivent etre E, N, S ou W\n");
        return CODE_CHEMIN_INVALIDE;

        case CODE_CHEMIN_TROP_LONG:
        printf("Erreur: la longueur ne doit pas depasser 40\n");
        return CODE_CHEMIN_TROP_LONG;

        case CODE_HAUTEUR_INVALIDE:
        printf("Erreur: la hauteur ne doit pas depasser 10\n");
        return CODE_HAUTEUR_INVALIDE;

        case CODE_LARGEUR_INVALIDE:
        printf("Erreur: la largeur ne doit pas depasser 15\n");
        return CODE_LARGEUR_INVALIDE;

        default:
        printf("L'exécution est un succès\n");
        return CODE_OK;
    }
}

// On parcourt le chemin en vérifiant que les deplacements correspondent à ceux attendu
int cheminEstValide(const char *chemin) {
    unsigned int i = 0;
    for (; i <= strlen(chemin)-1; i++) {
        if (chemin[i] != EST && chemin[i] != NORD && chemin[i] != OUEST && chemin[i] != SUD) {
            return 0;
        }
    }
    return 1;

}

/*
Initialisation du chemin, on verifie les deplacements par rapport  chemin,
si le déplacement est au SUD, le chemin se dirige vers la bas,
si le déplacement est au NORD, le chemin se dirige vers le haut,
si le déplacement est à l'EST, le chemin se dirige vers la droite,
si le déplacement est à l'OUEST, le chemin se dirige vers la gauche.
*/
void initialiserChemin(struct Chemin *chemin, const char *deplacements) {
    struct Rectangle* rectangle = &chemin->rectangle;
    rectangle->xmin = 0;
    rectangle->xmax = 0;
    rectangle->ymin = 0;
    rectangle->ymax = 0;

    int y = 0;
    int x = 0;
    unsigned int i = 0;
    for (; i <= strlen(deplacements)-1; i++) {
        if (deplacements[i] == SUD) {
            y++;
        } else if (deplacements[i] == NORD) {
            y--;
        } else if (deplacements[i] == EST) {
            x++;
        } else if (deplacements[i] == OUEST) {
            x--;
        }

        if (y >= rectangle->ymax) {
            rectangle->ymax = y;
        }
        if (y < rectangle->ymin) {
            rectangle->ymin = y;
        }
        if (x >= rectangle->xmax) {
            rectangle->xmax = x;
        }
        if (x < rectangle->xmin) {
            rectangle->xmin = x;
        }

        chemin->deplacements[i] = deplacements[i];
    }

    rectangle->xmax++;
    rectangle->ymax++;
}

/*
On initialise le dessin en parcourant le tableau cases, en modifiant son
contenu en mettant les elements de ces cases à vide.
*/
void initialiserDessin(struct Dessin *dessin) {
    unsigned int i = 0;
    unsigned int j = 0;
    for (; i < dessin->nbLignes; i++) {
        for (j = 0; j < dessin->nbColonnes; j++) {
            dessin->cases[i][j] = dessin->vide;

        }
    }

}

/*
affichage du dessin; on parcourt le dessin en affichant les éléments que ces
cases contiennent.
*/
void afficherDessin(const struct Dessin *dessin) {
    unsigned int i = 0;
    unsigned int j = 0;
    for (; i < dessin->nbLignes; i++) {
        for (j = 0; j < dessin->nbColonnes; j++) {
            printf("%c", dessin->cases[i][j]);
        }
        printf("\n");
    }
}

/*
Représentation du chemin sur le dessin;
le point de départ se trouve à la 3e ligne, 1re colonne
*/
void ajouterCheminDansDessin(struct Dessin *dessin, const struct Chemin *chemin) {
    int coordL = 3;
    if(abs(chemin->rectangle.ymin) < 3)
        coordL = abs(chemin->rectangle.ymin);
    int coordC = 0;

    unsigned int i = 0;
    for (; i <=strlen(chemin->deplacements); i++) {
        dessin->cases[coordL][coordC] = dessin->occupe;

        switch (chemin->deplacements[i]) {
            case NORD: coordL--;
            break;

            case SUD: coordL++;
            break;

            case EST: coordC++;
            break;

            case OUEST: coordC--;
        }

        if(coordC<0) {
            coordC = 0;
        }

        if(coordL<0) {
            coordL = 0;
        }
    }
}

/*
On parcourt le tableau, on verifie si il est occupé.
Toutes cases occupées ayant au plus 3 voisins qui sont aussi occupés est
considéré comme une intersection
*/
void identifierIntersections(struct Dessin *dessin) {
    int compteur = 0;

    unsigned int i = 0;
    unsigned int j = 0;
    for (; i < dessin->nbLignes; i++) {
        for (j = 0; j < dessin->nbColonnes; j++) {
            if(dessin->cases[i][j]==dessin->occupe) {
                compteur = 0;

                if (j > 0 && (dessin->cases[i][j-1]==dessin->occupe || dessin->cases[i][j-1]==dessin->intersection)) {
                    compteur++;
                }

                if (j < dessin->nbColonnes-1 && (dessin->cases[i][j+1]==dessin->occupe || dessin->cases[i][j+1]==dessin->intersection)) {
                    compteur++;
                }

                if (i > 0 && (dessin->cases[i-1][j]==dessin->occupe || dessin->cases[i-1][j]==dessin->intersection)) {
                    compteur++;
                }

                if (i < dessin->nbLignes-1 && (dessin->cases[i+1][j]==dessin->occupe || dessin->cases[i+1][j]==dessin->intersection)) {
                    compteur++;
                }

                if (compteur >= 3) {
                    dessin->cases[i][j] = dessin->intersection;
                }
            }
        }
    }
}

//On valide les arguments et on fait appel aux différentes méthodes.
int main(int argc, char const *argv[]) {
     if (argc != 5) {
         return afficherErreur(CODE_NOMBRE_ARGUMENTS_INVALIDE);
     }

     if (strlen(argv[1]) != 1 || strlen(argv[2]) != 1 || strlen(argv[3]) != 1) {
         return afficherErreur(CODE_CARACTERES_NON_UNIQUES);
     }

     if (strcmp(argv[1],argv[2]) == 0 || strcmp(argv[1],argv[3]) == 0 || strcmp(argv[2],argv[3]) == 0) {
         return afficherErreur(CODE_CARACTERE_VIDE_INVALIDE);
     }

     if (strlen(argv[4]) > MAX_CHEMIN) {
         return afficherErreur(CODE_CHEMIN_TROP_LONG);
     }

     if (!cheminEstValide(argv[4])) {
         return afficherErreur(CODE_CHEMIN_INVALIDE);
     }

     struct Chemin chemin;

     initialiserChemin(&chemin, argv[4]);

     if (chemin.rectangle.ymax-chemin.rectangle.ymin > MAX_LIGNES) {
         return afficherErreur(CODE_HAUTEUR_INVALIDE);
     }

     if (chemin.rectangle.xmax-chemin.rectangle.xmin > MAX_COLONNES) {
         return afficherErreur(CODE_LARGEUR_INVALIDE);
     }

     struct Dessin dessin;

      dessin.nbLignes = chemin.rectangle.ymax-chemin.rectangle.ymin;
      dessin.nbColonnes = chemin.rectangle.xmax-chemin.rectangle.xmin;
      dessin.occupe = *argv[1];
      dessin.vide = *argv[2];
      dessin.intersection = *argv[3];

     initialiserDessin(&dessin);

     ajouterCheminDansDessin(&dessin,&chemin);

     if (dessin.nbLignes > 1 && dessin.nbColonnes > 1) {
         identifierIntersections(&dessin);
     }

     afficherDessin(&dessin);

     return CODE_OK;
 }
